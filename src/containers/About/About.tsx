import * as React from "react";
import { AppTitle } from "@ui/title";

interface AppComponentProps {}
function About(props: AppComponentProps): JSX.Element {

  return (
    <div>
      
      <AppTitle data-testid="app-title">About page</AppTitle>
    </div>
  );
}

export default About