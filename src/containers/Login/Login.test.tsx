import * as React from "react";
import Login from "./Login";
import {
  render,
  fireEvent,
  cleanup,
  getByTestId,
  waitForElement
} from "react-testing-library";
import { LoginForm } from "@components/LoginForm/LoginForm";
import { testIdGetter } from "@mock/mock.utils";
import { API } from "@shared/const";
import { Post} from "@mock/fetch.mock";
import App from "../App/App";
import { renderWithRouter, getTestIdWithRouter } from "@mock/router.mock";

describe("Access to app", () => {
  // automatically unmount and cleanup DOM after the test is finished.
  afterEach(cleanup);

  test("It should renders without crashing", () => {
    const { getByTestId } = render(<Login />);
    const logo = getByTestId("login-logo");
    expect(logo).toBeDefined();
  });
  test("It should prevent submit", () => {
    const handleTryAuth = jest.fn();
    const loginType = "signin";
    const handleResetValidationError = jest.fn();
    let isInvalid = false;
    const { getByTestId, getByText } = render(
      <LoginForm
        id="login-form"
        loginType={loginType}
        tryAuth={handleTryAuth}
        resetValidationError={handleResetValidationError}
        validState={isInvalid}
      />
    );
    const MOCK_INPUT: string = "";
    const MOCK_PWD: string = "re";

    const { loginInput } = testIdGetter<HTMLInputElement>(
      ["loginInput", "login-input"],
      getByTestId
    );
    fireEvent.change(loginInput, { target: { value: MOCK_INPUT } });
    expect(loginInput.value).toBe(MOCK_INPUT);

    const { pwdInput } = testIdGetter<HTMLInputElement>(
      ["pwdInput", "pwd-input"],
      getByTestId
    );
    fireEvent.change(pwdInput, { target: { value: MOCK_PWD } });
    expect(pwdInput.value).toBe(MOCK_PWD);

    const { loginSubmit } = testIdGetter<HTMLButtonElement>(
      ["loginSubmit", "login-submit"],
      getByTestId
    );
    expect(loginSubmit.disabled).toEqual(true);

    getByText(/submit/i).click();
    //act
    expect(handleTryAuth).toHaveBeenCalledTimes(0);
  });

  test("It should prevent access",async  () => {
    const fakeAxios = {
      // get: jest.fn(() => Promise.resolve({data: {greeting: 'hello there'}})),
      post: jest.fn((cred) => Promise.resolve({data: {message: 'invalid'}})),
 
     }
     const url=API 
     const {getByText, getByTestId} = render(<Post url={url} axios={fakeAxios}  testId='message'  />)
     fireEvent.click(getByText(/fetch/i))
   
     const greetingNode = await waitForElement(() => getByTestId('message'))
     expect(fakeAxios.post).toHaveBeenCalledTimes(1)
     expect(fakeAxios.post).toHaveBeenCalledWith(url)
     expect(getByText('invalid').textContent).toBe('invalid') 

  })
  test("It should allow access", () => {
    const handleTryAuth = jest.fn();
    const loginType = "signin";
    const handleResetValidationError = jest.fn();

    const { getByTestId, rerender, getByText } = render(
      <LoginForm
        id="login-form"
        loginType={loginType}
        tryAuth={handleTryAuth}
        resetValidationError={handleResetValidationError}
        validState={false}
      />
    );
    const MOCK_INPUT: string = "toto";
    const MOCK_PWD: string = "rerere";

    const { loginInput } = testIdGetter<HTMLInputElement>(
      ["loginInput", "login-input"],
      getByTestId
    );
    fireEvent.change(loginInput, { target: { value: MOCK_INPUT } });
    expect(loginInput.value).toBe(MOCK_INPUT);

    const { pwdInput } = testIdGetter<HTMLInputElement>(
      ["pwdInput", "pwd-input"],
      getByTestId
    );
    fireEvent.change(pwdInput, { target: { value: MOCK_PWD } });

    const { loginSubmit } = testIdGetter<HTMLButtonElement>(
      ["loginSubmit", "login-submit"],
      getByTestId
    );

    // re-render
    rerender(
      <LoginForm
        id="login-form"
        loginType={loginType}
        tryAuth={handleTryAuth}
        resetValidationError={handleResetValidationError}
        validState={true}
      />
    );

    expect(loginSubmit.disabled).toEqual(false);
    getByText(/submit/i).click();
    //act
    expect(handleTryAuth).toHaveBeenCalledTimes(1);
    expect(handleTryAuth).toHaveBeenCalledWith({
      login: MOCK_INPUT,
      pwd: MOCK_PWD
    });

    //given component and route
   const routedApp= getTestIdWithRouter(<App />, '/', 'app-title')
    expect(routedApp.textContent).toBe('Stateless simple react hook starter')
  });
});
