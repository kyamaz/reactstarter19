import * as React from "react";
import { AppTitle } from "@ui/title";

interface AppComponentProps {}
function App(props: AppComponentProps): JSX.Element {

  return (
    <div>
      
      <AppTitle data-testid="app-title">Stateless simple react hook starter</AppTitle>
    </div>
  );
}

export default App;
