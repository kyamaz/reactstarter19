import * as React from "react";
import { render, fireEvent, cleanup, getByTestId } from "react-testing-library";

import App from "@containers/App/App";
import { getTestIdWithRouter } from "@mock/router.mock";
import { AppNav } from "./app-nav";
import { NavLinkItem } from "@ui/navbar";


describe("nav in app", () => {

  beforeEach(function () {
    // import and pass your custom axios instance to this method
  })

  afterEach(function () {
    // import and pass your custom axios instance to this method
    cleanup()
  })

  const LINKS_LENGTH = 1;
  // automatically unmount and cleanup DOM after the test is finished.
  afterEach(cleanup);
  test("It should links to all page", () => {
    const routedNav = getTestIdWithRouter(<AppNav />, "/", "links");
    expect(routedNav.children.length).toBe(LINKS_LENGTH);
  });
  test("It should nav to all", () => {
    const mountNavTestHome = getTestIdWithRouter(<AppNav />, "/", "links");
    //given component and route
    const routedLinkHome = getTestIdWithRouter(
      <NavLinkItem to="/" />,
      "/",
      "link-home"
    );
    routedLinkHome.click();
    const routedApp = getTestIdWithRouter(<App />, "/", "app-title");
    expect(routedApp.textContent).toBe("Stateless simple react hook starter");

  });
});
