import { createContext } from "react";

export const AppContext = createContext([{}, (): any => <any>{}]);
export const AppProvider: any = AppContext.Provider;
