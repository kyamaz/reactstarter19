import * as React from "react";
import { addLocaleData } from "react-intl";
import en from "react-intl/locale-data/en";
import de from "react-intl/locale-data/de";
import localData from "../assets/locales";

addLocaleData([...en, ...de]);
const FALLBACK_EN: string = "de";

export function useLocalLang(
  lang: string = FALLBACK_EN
): [string, { [key: string]: string }] {
  const [useLang, setUseLang] = React.useState(FALLBACK_EN);
  React.useEffect(() => {
    const locale = localData.hasOwnProperty(lang) ? lang : FALLBACK_EN;
    setUseLang(locale);
  }, [lang]);
  return [useLang, localData[useLang]];
}
