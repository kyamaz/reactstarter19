import * as React from "react";
import styled from "styled-components";
import { NavLink } from "react-router-dom";

export const NavHeader = styled.header.attrs({
  className: "navbar"
})<{}>`
  margin-top: 20px;
`;
export const NavSection = styled.section.attrs({
  className: "navbar-section"
})<{}>``;
export const NavLinkItem = styled(NavLink).attrs({
  className: "navbar-brand"
})<{}>`
  padding: 0 10px;
  color: white;
  &:visited {
    color: white;
  }
  &.active {
    text-decoration: underline;
  }
`;
