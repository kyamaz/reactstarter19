import { BG_BASE } from "./../shared/style";
import styled from "styled-components";

export const Columns = styled.div.attrs({ className: "columns" })``;

export const VerticalColumns = styled(Columns)`
  flex: 1;
  height: 100vh;
  display: flex;
  flex-direction: column;
`;

export const LoginHeroBg = styled.div<{ isExpanded: boolean }>`
  display: flex;
  flex-direction: column;
  background: ${BG_BASE};
  flex: 1;
  filter: blur(${props => (props.isExpanded ? "40px" : 0)});
`;

export const LoginTopBar = styled.div`
  background-color: rgba(0, 31, 48, 0.09);
  padding: 15px 10px;
  display: flex;
  justify-content: flex-end;
  align-items: center;
`;

export const ListWraper = styled.div`
  height: 100%;
  overflow: hidden;
  background: ${BG_BASE};
`;

export const SpaceBetween = styled.div`
  display: flex;
  justify-content: space-between;
`;
