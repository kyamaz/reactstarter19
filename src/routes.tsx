import * as React from "react";
import loadable from "loadable-components";

//lazy loaded routes
function Loading({ error }) {
  if (error) {
    console.error(error);
    return "Oh nooess!";
  } else {
    return <div />;
  }
}

//important side note
//loadable loading expects component. not just plain jsx
//component must have default export.not nammed export


export const AppContainer = loadable(() =>  import("./containers/App/App"), {
  LoadingComponent: () => null
});

export const LoginContainer = loadable(() =>  import("./containers/Login/Login"), {
  LoadingComponent: () => null
});
export const AboutContainer = loadable(() =>  import("./containers/About/About"), {
  LoadingComponent: () => null
});
