import * as React from "react";
import * as ReactDOM from "react-dom";
import { Fragment } from "react";
import registerServiceWorker from "./registerServiceWorker";
import { BrowserRouter, Link } from "react-router-dom";
import loadable from "loadable-components";

function AboutRender() {
  return (
    <div>
      {" "}
      <p>about </p>
    </div>
  );
}

//css library

import "./styles.css";
//style
import { GlobalStyle } from "@shared/style";
//navigation
import { Route, Switch, Router } from "react-router-dom";
import { history } from "./route-history";

//routes
import { AppContainer, LoginContainer, AboutContainer } from "./routes";
import ProtectedRoute from "./containers/ProtectedRoute/ProtectedRoute";
//translation
import { IntlProvider, addLocaleData } from "react-intl";
import { useLocalLang } from "@hooks/useLocalLang";

import { loadComponents } from "loadable-components";
import { getState } from "loadable-components";

window["snapSaveState"] = () => getState();

//use key for dinamic language selection https://github.com/yahoo/react-intl/wiki/Components
function AppWrap(): JSX.Element {
  const browserLang = navigator.language.split(/[-_]/)[0]; // language without region code
  const [locale, localData] = useLocalLang(browserLang);

  return (
    <IntlProvider locale={locale} key={locale} messages={localData}>
      <Fragment>
        <Route
          path="/about/"
          exact={true}
          component={AboutContainer}
          children={({ match }) => <Link to="/about/">About</Link>}
        />
        <Route
          path="/login/"
          exact={true}
          component={LoginContainer}
          children={({ match }) => <Link to="/login/">About</Link>}
        />

        <GlobalStyle />
        <Switch>
          <Route exact path="/about/" component={AboutContainer} />
          <Route exact path="/login/" component={LoginContainer} />

          {/*             <ProtectedRoute path="/" exact component={AppContainer} />

            <ProtectedRoute component={AppContainer} /> */}
        </Switch>
      </Fragment>
    </IntlProvider>
  );
}
ReactDOM.render(<AppWrap />, document.getElementById("root") as HTMLElement);
registerServiceWorker();
