type testKeys = [string, string];
//react testing library
export function testIdGetter<T extends HTMLElement>(
  keys: testKeys,
  fn: Function
): {
  [prop: string]: T;
} {
  const [objKey, testId] = keys;
  return {
    [objKey]: fn(testId)
  };
}
