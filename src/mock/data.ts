export const MOCK_WIDTHS_CHANGE = {
  airdate: 0.5,
  airstamp: 1,
  airtime: 1.5,
  id: 1,
  image: 1,
  name: 2,
  number: 1,
  runtime: 1,
  season: 1,
  summary: 2.5,
  url: 1,
  _links: 1
};
